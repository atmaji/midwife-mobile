import React, { useEffect } from 'react';
import { AsyncStorage, StyleSheet, Text, View } from 'react-native';

const HomeComponent = ({ navigation }) => {
    useEffect(() => {
        !AsyncStorage.getItem('key') && navigation.navigate('welcome')
    })
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerContent}>Welcome Midwife Mobile</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    header: {
        // borderTopWidth: 1,
        // borderColor:"silver",
        marginTop: 30,
        // backgroundColor: "#FFA1A1",
        height: 50,
    },
    headerContent: {
        fontSize: 20,
        marginTop: 10,
        color: "#FFA1A1",
    }
});

export default HomeComponent;
